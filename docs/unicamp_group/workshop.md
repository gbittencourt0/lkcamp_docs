## Configure your environment

For this workshop we prepared a docker image with everything you need installed
for you.
If you prefer installing yourself, follow the normal tutorial since "Booting".

Install docker in your distro.

For Ubuntu or Debian, use the following commands:

```bash
sudo apt update
sudo apt install docker-ce
sudo service docker start
```

For Arch Linux, use the following commands:

```bash
sudo pacman -S docker
sudo systemctl start docker.service
sudo systemctl enable docker.service
```

Now, lets launch docker and download the source code:

```bash
mkdir linux
sudo docker run -i -t --volume $(realpath linux):/linux -w /linux rocha1558/lkcamp-kerneltools:slim
docker# git clone --depth 1 -b staging-testing git://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git
```

## Build and Run

Lets compile and run using virtme

```bash
docker# cd /linux/staging
docker# virtme-configkernel --defconfig
docker# make -j$(nproc)
docker# virtme-run --kdir=.
root@(none):/# uname -r
```

## Make some change

Lets hack the kernel a bit:

To exit virtme, press ctrl+a x

```bash
docker# vim init/main.c
```

Search for the kernel_init() function and add the following line just after rcu_end_inkernel_boot(); 

```c++
pr_err("\n================\nHello LKCAMP !!!\n================\n\n"); 
```

Save and close the file and compile again:

```bash
docker# make -j$(nproc)
docker# virtme-run --kdir=.
```

You should see your print there :)

## Send your first patch

### Configure your mail

As it was already mentioned the kernel development is made through mail. So
setup your git to send mails. Edit the following file:

```bash
docker# cd /linux/staging && vim .git/config
```

And add the lines:


```
[sendemail]
    smtpEncryption = tls
    smtpServer = smtp.gmail.com
    smtpUser = your.email@gmail.com
    smtpServerPort = 587
[user]
    name = <your name>
    email = <your email>
```

### Testing git send-email

```bash
docker# echo -e "\nChecking if send-email works\n" >> README
docker# git add README && git commit -m "Testing git send-email"
docker# git send-email --suppress-cc=all --to=<your email> HEAD^
```

Check your mailing box

!!! Warning "Beware with gmail secure"
    Gmail has a security feature that block e-mail send. To disable this feature access de URL above and turn off.

    <https://myaccount.google.com/lesssecureapps>

!!! Warning "2FA Gmail"
    If you have 2FA enabled in your Gmail follow this tip:

    If you have multifactor authentication setup on your gmail account, you will need to generate an app-specific password for use with git send-email. Visit <https://security.google.com/settings/security/apppasswords> to create it.
    Then you can add "smtpPass = Your password" in your .git/config

!!! Warning "If you use other e-mail domain"
    Adapt your configuration.

### Finding something to contribute

For the first contribution lets start easy and correct a simple thing as a typo inside the staging folder.

The staging folder contains temporary drivers that doesn't meet the kernel quality standards.
Once those drivers are fixed, they are migrated to the drivers/ folder.
So this is a great place to start, as there are several minor things to fix, and the community is used
to receive patches from new contributors through the staging tree.

To find something to fix, there is a script called checkpatch that shows you several style errors in the file:

```bash
docker# cd /linux/staging
docker# scripts/checkpatch.pl -f drivers/staging/isdn/hysdn/hycapi.c
...
ERROR: that open brace { should be on the previous line
#312: FILE: drivers/staging/isdn/hysdn/hycapi.c:312:
+       if (chk == 1)
+       {
}}
...
```

### Sending your first patch to the maintainer

Inside the kernel repo add your change to stage:

```bash
git add <the_file_you_changed>
```

Commit your changes:

```bash
git commit -s
```

To see how you should format your commit message, check how other commits were
previously made in this same file.

To check this, you can go to:

`https://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git/log/<path to the file you changed>`

Example: <https://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git/log/drivers/staging/android/vsoc.c>

Check how in this case, all commits starts with `staging: android: vsoc:`, so other changes to this file should also follow this model.

Also, you should mention which error from Checkpath you are fixing, check this example <https://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git/commit/?id=e4d7efbb91efc3040e3f1c645571c1769095ae79>

```diff
From	Beatriz Martins de Carvalho <martinsdecarvalhobeatriz@gmail.com>
Subject	[PATCH v2] staging: rtl8723bs: hal: replace spaces by tabs.
Date	Thu, 4 Apr 2019 10:09:30 -0300

Fix checkpatch error "ERROR: code indent should use tabs where possible"
in hal_com_phycfg.c:1726.

Signed-off-by: Beatriz Martins de Carvalho <martinsdecarvalhobeatriz@gmail.com>
---
Changes in v2:
	- correction commit title
	- added more information about the error
---
 drivers/staging/rtl8723bs/hal/hal_com_phycfg.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/drivers/staging/rtl8723bs/hal/hal_com_phycfg.c b/drivers/staging/rtl8723bs/hal/hal_com_phycfg.c
index 828b328adab8..fc49cdfcd477 100644
--- a/drivers/staging/rtl8723bs/hal/hal_com_phycfg.c
+++ b/drivers/staging/rtl8723bs/hal/hal_com_phycfg.c
@@ -1723,7 +1723,7 @@ s8 phy_get_tx_pwr_lmt(struct adapter *adapter, u32 reg_pwr_tbl_sel,
 	idx_rate_sctn = get_rate_sctn_idx(data_rate);
 
 	if (band_type == BAND_ON_5G && idx_rate_sctn == 0)
-                DBG_871X("Wrong rate 0x%x: No CCK in 5G Band\n", DataRate);
+		DBG_871X("Wrong rate 0x%x: No CCK in 5G Band\n", DataRate);
 
 	/*  workaround for wrong index combination to obtain tx power limit, */
 	/*  OFDM only exists in BW 20M */
-- 
2.17.1
```

Checkpatch your patch first and solve the errors:

```bash
perl scripts/checkpatch.pl -g HEAD
```

Search maintainer's email, in this case catch the mailing list e-mail:

```bash
git show | perl scripts/get_maintainer.pl --nokeywords     \
                                          --nogit          \
                                          --nogit-fallback \
                                          --norolestats
```

Check how the patch would be sent:

```bash
git send-email --annotate --suppress-cc=all --dry-run HEAD^
```

Don't forget to add the emails from the maintainers.

Now, send to your first patch to the maintainer:

```bash
git send-email --annotate --suppress-cc=all HEAD^
```
