FROM python:alpine

RUN set -ex ;\
    apk add --no-cache bash ;\
    pip install mkdocs ;\
    pip install mkdocs-material ;\
    pip install pymdown-extensions

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod a+rx usr/local/bin/docker-entrypoint.sh ;\
    ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 8000
CMD ["mkdocs", "serve", "-a", "0.0.0.0:8000"]
